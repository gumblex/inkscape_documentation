# translation of Inkscape tutorial tracing to Slovak
# Copyright (C)
# This file is distributed under the same license as the Inkscape package.
# Peter Mráz <etkinator@gmail.com>, 2009.
# Ivan Masár <helix84@centrum.sk>, 2009, 2010, 2015, 2016.
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tracing\n"
"POT-Creation-Date: 2021-03-19 18:35+0100\n"
"PO-Revision-Date: 2016-12-29 13:55+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Virtaal 0.7.1\n"
"X-Poedit-Language: Slovak\n"
"X-Poedit-Country: SLOVAKIA\n"

#: tracing-f06.svg:49(format) tracing-f05.svg:49(format)
#: tracing-f04.svg:49(format) tracing-f03.svg:49(format)
#: tracing-f02.svg:49(format) tracing-f01.svg:49(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: tracing-f06.svg:70(tspan) tracing-f05.svg:70(tspan)
#: tracing-f04.svg:70(tspan) tracing-f03.svg:70(tspan)
#: tracing-f02.svg:70(tspan)
#, no-wrap
msgid "Original Image"
msgstr "Pôvodný obrázok"

#: tracing-f06.svg:81(tspan)
#, no-wrap
msgid "Traced Image / Output Path - Simplified"
msgstr "Vektorizovaný obrázok - Zjednodušenie"

#: tracing-f06.svg:86(tspan)
#, no-wrap
msgid "(384 nodes)"
msgstr "(384 uzlov)"

#: tracing-f05.svg:81(tspan)
#, no-wrap
msgid "Traced Image / Output Path"
msgstr "Vektorizovaný obrázok"

#: tracing-f05.svg:86(tspan)
#, no-wrap
msgid "(1,551 nodes)"
msgstr "(1 551 uzlov)"

#: tracing-f04.svg:81(tspan) tracing-f04.svg:97(tspan)
#, no-wrap
msgid "Quantization (12 colors)"
msgstr "Prerozdelenie (12 farieb)"

#: tracing-f04.svg:86(tspan) tracing-f03.svg:86(tspan)
#: tracing-f02.svg:86(tspan)
#, no-wrap
msgid "Fill, no Stroke"
msgstr "Výplň bez ťahu"

#: tracing-f04.svg:102(tspan) tracing-f03.svg:102(tspan)
#: tracing-f02.svg:102(tspan)
#, no-wrap
msgid "Stroke, no Fill"
msgstr "Ťah bez výplne"

#: tracing-f03.svg:81(tspan) tracing-f03.svg:97(tspan)
#, no-wrap
msgid "Edge Detected"
msgstr "Detekované hrany"

#: tracing-f02.svg:81(tspan) tracing-f02.svg:97(tspan)
#, no-wrap
msgid "Brightness Threshold"
msgstr "Orezanie jasu"

#: tracing-f01.svg:70(tspan)
#, no-wrap
msgid "Main options within the Trace dialog"
msgstr "hlavné možnosti v dialógu vektorizácie"

#: tutorial-tracing.xml:6(title)
msgid "Tracing bitmaps"
msgstr "Vektorizácia bitmáp"

#: tutorial-tracing.xml:7(subtitle)
msgid "Tutorial"
msgstr ""

#: tutorial-tracing.xml:11(para)
msgid ""
"One of the features in Inkscape is a tool for tracing a bitmap image into a "
"&lt;path&gt; element for your SVG drawing. These short notes should help you "
"become acquainted with how it works."
msgstr ""
"Jednou z funkcií v programe Inkscape je nástroj na vektorizáciu rastrového "
"obrázka na prvok &lt;path&gt; (cesta) v SVG kresbe. Tento krátky návod vám "
"pomôže pochopiť, ako tento nástroj pracuje."

#: tutorial-tracing.xml:16(para)
msgid ""
"Currently Inkscape employs the Potrace bitmap tracing engine (<ulink url="
"\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>) by Peter "
"Selinger. In the future we expect to allow alternate tracing programs; for "
"now, however, this fine tool is more than sufficient for our needs."
msgstr ""
"V súčasnosti nástroj  používa na vektorizáciu algoritmus Potrace (<ulink url="
"\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>) od Petra "
"Selingera. V budúcnosti plánujeme pridanie aj ďalších vektorizačných "
"programov, ale na začiatok je tento nástroj viac ako postačujúci."

#: tutorial-tracing.xml:23(para)
msgid ""
"Keep in mind that the Tracer's purpose is not to reproduce an exact "
"duplicate of the original image; nor is it intended to produce a final "
"product. No autotracer can do that. What it does is give you a set of curves "
"which you can use as a resource for your drawing."
msgstr ""
"Majte na pamäti, že úlohou Vektorizátora nie je reprodukovať originálny "
"obrázok, ani to, aby vytvoril finálny produkt. Žiadny automatický "
"vektorizačný program to nedokáže. To, čo vedia takéto programy poskytnúť, je "
"sada kriviek, ktoré môžete použiť ako základ pri kreslení."

#: tutorial-tracing.xml:28(para)
msgid ""
"Potrace interprets a black and white bitmap, and produces a set of curves. "
"For Potrace, we currently have three types of input filters to convert from "
"the raw image to something that Potrace can use."
msgstr ""
"Algoritmus Potrace interpretuje čiernobielu bitmapu a vyprodukuje sadu "
"kriviek. Pred použitím algoritmu je potrebné použiť jeden z troch dostupných "
"filtrov, ktoré prevedú pôvodný obrázok na niečo, čo Potrace dokáže spracovať."

#: tutorial-tracing.xml:32(para)
msgid ""
"Generally the more dark pixels in the intermediate bitmap, the more tracing "
"that Potrace will perform. As the amount of tracing increases, more CPU time "
"will be required, and the &lt;path&gt; element will become much larger. It "
"is suggested that the user experiment with lighter intermediate images "
"first, getting gradually darker to get the desired proportion and complexity "
"of the output path."
msgstr ""
"Vo všeobecnosti platí, že čím viac tmavých pixlov je v pomocnom obrázku, tým "
"dlhšie bude Portrace vykonávať vektorizáciu. Dlhšia vektorizácia znamená aj "
"väčšiu záťaž procesora a prvok &lt;path&gt; bude tiež oveľa dlhší. Odporúča "
"sa, aby používateľ na začiatku experimentálne nastavil svetlejší pomocný "
"obrázok a potom ho postupne stmavoval, až kým nezíska požadované proporcie a "
"úplnú výstupnú cestu."

#: tutorial-tracing.xml:38(para)
#, fuzzy
msgid ""
"To use the tracer, load or import an image, select it, and select the "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Trace Bitmap</guimenuitem></"
"menuchoice> item, or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"alt\">Alt</keycap><keycap>B</keycap></keycombo>."
msgstr ""
"Ak chcete použiť vektorizáciu, nahrajte alebo importujte obrázok, označte ho "
"a potom vyberte z ponuky položku <command>Cesta &gt; Vektorizovať bitmapu</"
"command> alebo stlačte <keycap>Shift+Alt+B</keycap>."

#: tutorial-tracing.xml:50(para)
msgid "The user will see the three filter options available:"
msgstr "Uvidíte tri filtre, ktoré si môžete vybrať:"

#: tutorial-tracing.xml:55(para)
msgid "Brightness Cutoff"
msgstr "Orezanie jasu"

#: tutorial-tracing.xml:60(para)
msgid ""
"This merely uses the sum of the red, green and blue (or shades of gray) of a "
"pixel as an indicator of whether it should be considered black or white. The "
"threshold can be set from 0.0 (black) to 1.0 (white). The higher the "
"threshold setting, the fewer the number pixels that will be considered to be "
"“white”, and the intermediate image with become darker."
msgstr ""
"Tento filter používa iba súčet odtieňov červenej, zelenej a modrej (alebo "
"odtieňov šedej) pixla na to, aby určil, či pixel bude priradený k čiernej "
"alebo bielej farbe. Prah môže byť nastavený od 0,0 (čierna) po 1,0 (biela). "
"Vyššia hodnota prahu zníži počet pixlov priradených k „bielej“ farbe a "
"spôsobí to, že pomocný obrázok bude tmavší."

#: tutorial-tracing.xml:75(para)
msgid "Edge Detection"
msgstr "Detekcia hrán"

#: tutorial-tracing.xml:80(para)
msgid ""
"This uses the edge detection algorithm devised by J. Canny as a way of "
"quickly finding isoclines of similar contrast. This will produce an "
"intermediate bitmap that will look less like the original image than does "
"the result of Brightness Threshold, but will likely provide curve "
"information that would otherwise be ignored. The threshold setting here (0.0 "
"– 1.0) adjusts the brightness threshold of whether a pixel adjacent to a "
"contrast edge will be included in the output. This setting can adjust the "
"darkness or thickness of the edge in the output."
msgstr ""
"Tento filter používa algoritmus na detekciu hrán od J. Cannyho, ktorý dokáže "
"rýchlo nájsť rozhranie medzi plochami s podobným kontrastom. Vytvára pomocný "
"obrázok, ktorý je veľmi podobný tomu, ktorý vyprodukuje filter Orezanie "
"hrán, ale poskytuje viac informácií o krivkách, ktoré by boli inak "
"ignorované. Nastavenie prahu pri tomto filtri (0,0 – 1,0) upravuje rozhranie "
"jasu, ktoré určuje, či pixel susediaci s hranicou kontrastov bude zahrnutý "
"do výstupu. Toto nastavenie môže upravovať hrúbku a sýtosť výslednej čiary."

#: tutorial-tracing.xml:96(para)
msgid "Color Quantization"
msgstr "Prerozdelenie farieb"

#: tutorial-tracing.xml:101(para)
msgid ""
"The result of this filter will produce an intermediate image that is very "
"different from the other two, but is very useful indeed. Instead of showing "
"isoclines of brightness or contrast, this will find edges where colors "
"change, even at equal brightness and contrast. The setting here, Number of "
"Colors, decides how many output colors there would be if the intermediate "
"bitmap were in color. It then decides black/white on whether the color has "
"an even or odd index."
msgstr ""
"Tento filter ako výsledok vytvorí pomocný obrázok, ktorý sa podstatne líši "
"od predchádzajúcich dvoch, ale je aj tak veľmi potrebný. Miesto toho, aby "
"vytváral oddeľujúce čiary medzi plochami s rozdielnym jasom alebo "
"kontrastom, nájde okraje farebných plôch, a to aj takých dvoch farieb, ktoré "
"majú zhodný jas a kontrast. Nastavenie Počet farieb určuje, koľko výstupných "
"farieb bude vytvorených v prípade, že bitmapa je farebná. To, či bude farba "
"priradená k bielej alebo čiernej, sa rozhodne podľa toho, či index farby je "
"párny alebo nepárny."

#: tutorial-tracing.xml:115(para)
msgid ""
"The user should try all three filters, and observe the different types of "
"output for different types of input images. There will always be an image "
"where one works better than the others."
msgstr ""
"Môžete vyskúšať všetky tri filtre a preskúmať rozdielnosť typov výstupov s "
"rôznymi typmi vstupných obrázkov. Pri každom obrázku zistíte, že jeden z "
"filtrov sa hodí viac ako tie ostatné."

#: tutorial-tracing.xml:119(para)
#, fuzzy
msgid ""
"After tracing, it is also suggested that the user try "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Simplify</guimenuitem></"
"menuchoice> (<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</"
"keycap></keycombo>) on the output path to reduce the number of nodes. This "
"can make the output of Potrace much easier to edit. For example, here is a "
"typical tracing of the Old Man Playing Guitar:"
msgstr ""
"Po vektorizácii sa odporúča, aby používateľ skúsil zredukovať počet uzlov v "
"ceste pomocou položky <command>Cesta &gt; Zjednodušiť</command> alebo "
"stlačením (<keycap>Ctrl+L</keycap>). To môže významne zjednodušiť "
"upravovanie výstupu algoritmu Potrace. Tu je typický príklad vektorizácie "
"obrazu Starec hrajúci na gitare:"

#: tutorial-tracing.xml:133(para)
#, fuzzy
msgid ""
"Note the enormous number of nodes in the path. After hitting "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo>, this is a typical result:"
msgstr ""
"Všimnite si nadmerný počet uzlov v ceste. Po stlačení <keycap>Ctrl+L</"
"keycap> sa počet zvyčajne zníži:"

#: tutorial-tracing.xml:144(para)
msgid ""
"The representation is a bit more approximate and rough, but the drawing is "
"much simpler and easier to edit. Keep in mind that what you want is not an "
"exact rendering of the image, but a set of curves that you can use in your "
"drawing."
msgstr ""
"Reprezentácia je trochu skreslená a drsná, ale kresba sa bude oveľa ľahšie "
"upravovať. Nezabudnite, že to, čo sme chceli dosiahnuť, nie je presné "
"vykreslenie obrázka, ale sada kriviek, ktoré môžeme použiť pri kreslení."

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-tracing.xml:0(None)
msgid "translator-credits"
msgstr ""
"Peter Mráz <etkinator@gmail.com>, 2009\n"
"Ivan Masár <helix84@centrum.sk>, 2009, 2010, 2015"

#~ msgid "Optimal Edge Detection"
#~ msgstr "Optimálna detekcia hrán"
